import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import TopNewsScreen from '../screens/App/TopNews/TopNewsScreen';
import NewsDetailsScreen from '../screens/App/NewsDetails/NewsDetailsScreen'
import Header from '../components/Header';

const TopNewsNavigator = createStackNavigator({
    TopNews: {
        screen: TopNewsScreen,
        navigationOptions: ({ navigation }) => ({
            header: <Header
                isHome
                onPress={() => navigation.toggleDrawer()}
            />,
        }),
    },
    NewsDetails: {
        screen: NewsDetailsScreen,
        navigationOptions: ({ navigation }) => ({
            header: <Header
                title='News Details'
                onPress={() => navigation.pop()}
            />,
        }),
    },
}, {
    initialRouteName: 'TopNews'
}
);

export default TopNewsNavigator;
