import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import CustomNewsScreen from '../screens/App/CustomNews/CustomNewsScreen';
import NewsDetailsScreen from '../screens/App/NewsDetails/NewsDetailsScreen';
import Header from '../components/Header';

const CustomNewsNavigator = createStackNavigator({
    CustomNews: {
        screen: CustomNewsScreen,
        navigationOptions: ({ navigation }) => ({
            header: <Header
                isHome
                onPress={() => navigation.toggleDrawer()}
            />,
        }),
    },
    NewsDetails: {
        screen: NewsDetailsScreen,
        navigationOptions: ({ navigation }) => ({
            header: <Header
                title='News Details'
                onPress={() => navigation.pop()}
            />,
        }),
    },
}, {
    initialRouteName: 'CustomNews'
}
);

export default CustomNewsNavigator;
