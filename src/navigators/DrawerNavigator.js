import React from 'react';
import { View, Dimensions } from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Icon } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import MainBottomTabNavigator from './MainBottomTabNavigator';
import colors from '../config/colors';
import { CustomeIcon } from '../config';
import CustomDrawerContentComponent from '../components/CustomDrawerMenu';


const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });


export const DrawerNavigator = createDrawerNavigator(
    {
        Home: {
            screen: MainBottomTabNavigator,
            headerMode: 'none',
            header: null,
            navigationOptions: {
                drawerLabel: 'News ',
                drawerIcon: ({ tintColor }) => (
                    <Icon name={'description'} type={'MaterialIcons'} style={{ fontSize: EStyleSheet.value('22rem'), color: tintColor, }} />
                ),
            }
        },
    },
    {
        initialRouteName: 'Home',
        drawerType: 'front',
        drawerBackgroundColor: colors.primary,
        backBehavior: 'none',
        contentComponent: CustomDrawerContentComponent,
        contentOptions: {
            activeTintColor: colors.white,
            inactiveTintColor: 'rgba(255, 255, 255, 0.5)',
            itemsContainerStyle: {
                marginVertical: 0,
            },
            iconContainerStyle: {
                opacity: 1
            }
        }
    }
);

