import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import Header from '../components/Header';
import TermsPrivacyHeader from '../components/TermsPrivacyHeader';
import Tutorial from '../screens/App/Drawer/Tutorial';
import NotificationsScreen from '../screens/App/Drawer/MyAccount/NotificationsScreen';
import TermsAndConditions from '../screens/App/Drawer/TermsAndConditions';
import PrivacyPolicy from '../screens/App/Drawer/PrivacyPolicy';
import About from '../screens/App/Drawer/About';

export const TutorialStackNavigator = createStackNavigator(
    {
        Tutorial: {
            screen: Tutorial,
            navigationOptions: ({ navigation }) => ({
                header: <Header
                    title='Tutorial'
                    onPress={() => navigation.navigate('Home')}
                />,
            }),
        }
    },
    {
        initialRouteName: 'Tutorial',
    }
);

export const NotificationsStackNavigator = createStackNavigator(
    {
        Notifications: {
            screen: NotificationsScreen,
            navigationOptions: ({ navigation }) => ({
                header: <Header
                    title='Notifications'
                    onPress={() => navigation.navigate('Home')}
                />,
            }),
        }
    },
    {
        initialRouteName: 'Notifications',
    }
);

export const TermsStackNavigator = createStackNavigator(
    {
        TermsAndConditions: {
            screen: TermsAndConditions,
            navigationOptions: ({ navigation }) => ({
                header: <Header
                    title='Terms and Conditions'
                    onPress={() => navigation.navigate('Home')}
                />,
            }),
        }
    },
    {
        initialRouteName: 'TermsAndConditions',
    }
);

export const PrivacyStackNavigator = createStackNavigator(
    {
        PrivacyPolicy: {
            screen: PrivacyPolicy,
            navigationOptions: ({ navigation }) => ({
                header: <Header
                    title='Privacy Policy'
                    onPress={() => navigation.navigate('Home')}
                />,
            }),
        }
    },
    {
        initialRouteName: 'PrivacyPolicy',
    }
);


export const AboutStackNavigator = createStackNavigator(
    {
        About: {
            screen: About,
            navigationOptions: ({ navigation }) => ({
                header: <Header
                    title='About'
                    onPress={() => navigation.navigate('Home')}
                />,
            }),
        }
    },
    {
        initialRouteName: 'About',
    }
);
