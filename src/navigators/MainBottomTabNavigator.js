import React from 'react';
import { View, Dimensions } from 'react-native';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Icon } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProfileStackNavigator from './ProfileStackNavigator';
import TopNewsNavigator from './TopNewsNavigator';
import CustomNewsNavigator from './CustomNewsNavigator';
import colors from '../config/colors';
import fontelloConfig from '../config/selection.json';

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });

export default createMaterialBottomTabNavigator(
    {
        TopNews: {
            screen: TopNewsNavigator,
            navigationOptions: {
                tabBarLabel: 'Top News',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon name={'description'} type={'MaterialIcons'} style={{ fontSize: EStyleSheet.value('22rem'), color: tintColor, }} />
                    </View>
                ),
            }
        },
        CustomNews: {
            screen: CustomNewsNavigator,
            navigationOptions: {
                tabBarLabel: 'Custom News',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon name={'language'} type={'MaterialIcons'} style={{ fontSize: EStyleSheet.value('22rem'), color: tintColor, }} />
                    </View>
                ),
            },
        },
        MyProfile: {
            screen: ProfileStackNavigator,
            navigationOptions: {
                tabBarLabel: 'My Profile',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon name={'face'} type={'MaterialIcons'} style={{ fontSize: EStyleSheet.value('22rem'), color: tintColor, }} />
                    </View>
                ),
            },
        },
    },
    {
        initialRouteName: 'TopNews',
        activeTintColor: colors.yellowTab,
        inactiveTintColor: colors.white,
        barStyle: { backgroundColor: colors.primary },
        shifting: false,
    },
);
