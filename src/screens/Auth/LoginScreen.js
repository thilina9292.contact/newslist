/* eslint-disable no-undef */
import React, { Component } from 'react';
import { View, ImageBackground, TextInput,TouchableHighlight,Text,StyleSheet,Keyboard} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../redux/actions';
import bg from '../../asserts/img/login_screen.png';
import colors from '../../config/colors';
import strings from '../../config/strings'
import { REG_EMAIL_ADDRESS } from '../../config/constants';
import Toast, {DURATION} from 'react-native-easy-toast';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../../services/NavigationService';

class LoginScreen extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password : '',
        };
    }
    
    initialUserLogin() {
      Keyboard.dismiss();
  
      const email = this.state.email;
      const password = this.state.password;

      if (!email || email.length === 0 || !password || password.length === 0) {
        this.refs.toast.show(strings.toastMessage.userDeatilsError, 1500, () => {
        });
        return;
      }else if(!this.validateEmail(email)){
        this.refs.toast.show(strings.toastMessage.userDeatilsEmailError, 1500, () => {
        });
        return;
      }else{
        this.refs.toast.show(strings.toastMessage.userloginSuccess, 1500, () => {
        });
        AsyncStorage.setItem('registrationStatus', 'true');
        AsyncStorage.setItem('userEmail',email);
        NavigationService.navigate('App');
      }

    }

    validateEmail(text) {
      return REG_EMAIL_ADDRESS.test(text);
    }

    render() {
        const {email, password} = this.state;
        return (
            <ImageBackground
                style={styles.backgroundImage}
                source={bg}>
                <View style={styles.container}>
                    <Toast ref="toast"/>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.inputs}
                        placeholder="Email"
                        placeholderTextColor = {colors.black}
                        keyboardType="email-address"
                        underlineColorAndroid="transparent"
                        autoCapitalize="none"
                        value={email}
                        onChangeText={email => this.setState({email})}
                    />
                </View>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.inputs}
                        placeholder="Password"
                        placeholderTextColor = {colors.black}
                        secureTextEntry={true}
                        value={password}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={password => this.setState({password})}
                        />
                </View>
                </View>

                <TouchableHighlight
                    underlayColor={colors.primaryLight}
                    style={[styles.buttonContainer, styles.loginButton]}
                    onPress={() => this.initialUserLogin()}>
                    <Text style={styles.loginText}>Login</Text>
                </TouchableHighlight>
        </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    inputContainer: {
      borderColor: colors.primary,
      backgroundColor: colors.white,
      borderRadius: 30,
      borderWidth: 2,
      width: '90%',
      height: 45,
      marginBottom: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    inputs: {
      height: 45,
      marginLeft: 16,
      flex: 1,
      fontSize : 14
    },
    buttonContainer: {
      width: '100%',
      height: 60,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: 0,
    },
    loginButton: {
      backgroundColor: colors.primary,
    },
    loginText: {
      color: colors.white,
      fontSize: 18,
    },
    backgroundImage: {
      flex: 1,
    },
  });

const mapStateToProps = state => {
    return {
    };
};

export default connect(mapStateToProps, actions)(LoginScreen);
