import React, { Component } from 'react';
import { View, Dimensions,FlatList } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import colors from '../../../config/colors';
import * as actions from '../../../redux/actions';
import Loader from '../../../components/Loader';
import NewsListItem from '../../../components/NewsListItem';

const entireScreenWidth = Dimensions.get('window').width;
const entireScreenHeight = Dimensions.get('window').height;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });

class TopNewsScreen extends Component {
    constructor(props) {
        super(props);
        this.page = 1;
        this.onEndReachedCalledDuringMomentum = true;
        this.state = {
            isFetching: false,
            newsList : [],
        };
    }

    async componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevProps.topNews !== this.props.topNews && this.props.topNews &&
            this.props.topNews.length > 0) {

                const prevItemList = this.state.newsList;
                const newItemList = this.props.topNews;
                const existingItemList = [...(prevItemList)];
                existingItemList.push(...(newItemList));
                this.setState({
                    newsList: existingItemList
                });
        }

        if (this.state.isFetching) {
            this.setState({
                isFetching : false,
            });
        }
    }

    fetchData() {
        this.page = 1;
        this.props.getTopNewsData(this.page);
    }

    handleLoadMore = ({ distanceFromEnd }) => { 
        let topNewsTotalResults =this.props.topNewsTotalResults;
        let topNews =this.props.topNews;

        let pageValue = Math.ceil(topNewsTotalResults/20) ;

        if (this.props.topNews 
            && this.props.topNews.length > 0 
            && !this.onEndReachedCalledDuringMomentum 
            && this.page < pageValue) {
    
            this.page = this.page + 1;
            this.props.getTopNewsData(this.page);
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    onRefresh() {
        this.setState({ isFetching: true,newsList:[]}, function() {
            this.page = 1;
            this.fetchData();
        });
       
    }

    renderItem(item) {
        console.log("thilina",item)
        return (
            <NewsListItem
                newsName={`${item.item.title}`}
                newsImage={`${item.item.urlToImage}`}
                defaultImage ={false}
                onPress={() => this.onNewsSelect(item)}
            />
        );
    }

    onNewsSelect(item) {
        this.props.navigation.navigate('NewsDetails', {
            newsItem: item,
        })
    }

    render() {
        let topNewsLoading = this.props.topNewsLoading;
        let newsList = this.state.newsList
        let isFetching = this.state.isFetching;

        return (
            <View style={styles.container}>
                {
                    (
                        <FlatList
                              data={newsList}
                              renderItem={this.renderItem.bind(this)}
                              showsVerticalScrollIndicator={false}
                              contentContainerStyle={{
                                 justifyContent: 'center',
                                  paddingHorizontal: EStyleSheet.value('4rem'),
                                 marginVertical: EStyleSheet.value('4rem')
                              }}
                              onEndReached={this.handleLoadMore.bind(this)}
                              onEndReachedThreshold={0.4}
                              extraData={this.state}
                              onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                              onRefresh={() => this.onRefresh()}
                              refreshing={isFetching}
                        />
                    )}
                {topNewsLoading && <Loader/>}
            </View>
        );
    }
}

const styles = EStyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
    },
});


const mapStateToProps = state => {
    return {
        topNews : state.news.topNews,
        topNewsLoading : state.news.topNewsLoading,
        topNewsTotalResults : state.news.topNewsTotalResults,
    };
};

export default connect(mapStateToProps, actions)(TopNewsScreen);