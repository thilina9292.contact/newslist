import React, { Component } from 'react';
import { View, Dimensions,ScrollView,Text,Image,Linking,TouchableHighlight } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import * as actions from '../../../redux/actions';
import _ from 'lodash';
import moment from "moment";
import colors from '../../../config/colors';
import strings from '../../../config/strings';
import {
    Card,
} from '../../../components';
import newsDefaultImg from '../../../asserts/img/defult_news.png';

const entireScreenWidth = Dimensions.get('window').width;
const entireScreenHeight = Dimensions.get('window').height;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });

class NewsDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newsItem:{}
        };
    }

    componentDidMount(){
        const {navigation: {state: {params}}} = this.props;
        newsItem = params.newsItem;
        this.setState({
            newsItem : newsItem,
        });    
    }

    openURL(newURL) {
        Linking.openURL(newURL);
    }

    render() {
        let newsItem =this.state.newsItem;
        if (!(_.isEmpty(newsItem))) {
            let newsDetails = newsItem.item.description
            let newsTitle = newsItem.item.title !== undefined ? 
                            newsItem.item.title : 
                            newsItem.item.category.toUpperCase() + ' '
                            + strings.news.newsDetailsDataTitle;

            let defaultImage  =  _.isEmpty(newsItem.item.urlToImage)  ? false : true;
            let imgURL = newsItem.item.urlToImage ? newsItem.item.urlToImage : '';
            let newURL = newsItem.item.url;

            return (
                <View style={styles.container}>
                    <View style={styles.newsImageContainer}>
                        <Card containerStyle={styles.card}>
                            <Text style={styles.title}>{newsTitle}</Text>
                            {
                                !defaultImage ? 
                                    <Image
                                        source={newsDefaultImg}
                                        resizeMode={'center'}
                                        style={[styles.image]}
                                    /> 
                                :
                                    <Image
                                        source={{uri: imgURL}}
                                        resizeMode={'center'}
                                        style={[styles.image]}
                                    /> 
                            }
                             <TouchableHighlight
                                underlayColor={colors.lightBlue}
                                onPress={() => this.openURL(newURL)}>
                                <Text style={styles.showURL}>{strings.news.newsDetailsOpenURL}</Text>
                             </TouchableHighlight>
                            
                        </Card>
                    </View>

                    <View style={styles.newsDataContainer}>
                        <ScrollView style={{flex:1}}>
                            <View>
                                <Card containerStyle={styles.card}>
                                    <Text style={styles.details}>{strings.news.newsDetailsData}</Text>
                                    {
                                        <Text style={styles.textContent}>{newsDetails}</Text>           
                                    }
                                </Card>
                            </View>

                        </ScrollView>
                        
                    </View>
                </View>
            );
        }else{
            console.log("empty",newsItem)
            return (
                <View style={styles.container}>
                </View>
            );
        }

        
    }
}

const styles = EStyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor : colors.white
    },
    newsImageContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor : colors.white,
        alignItems: 'center',
        marginTop: '5rem' 
    },
    newsDataContainer: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor : colors.white
    },
    card: {
        marginTop: '8rem',
        paddingVertical: '10rem',
        marginVertical: '15rem',
        paddingHorizontal: '10rem'
    },
    title: {
        fontSize: '18rem',
        color: colors.textNewsDetailsHeader,
        fontWeight: '600',
        textAlignVertical: "center",
        textAlign: "center"
    },
    showURL: {
        fontSize: '14rem',
        color: colors.showMoreNewsDeatls,
        fontWeight: '500',
        textAlignVertical: "center",
        textAlign: "center",
        textDecorationLine: 'underline',
    },
    details: {
        fontSize: '16rem',
        color: colors.textNewsDetailsHeader,
        paddingVertical: '8rem',
        fontWeight: '600',
    },
    textContent: {
        color: colors.textNewsDetails,
        fontSize: '14rem',
        fontWeight: '600',
        lineHeight: 21,
    },
    image: {
        width: '126rem',
        height: '126rem',
        alignSelf: 'center',
        resizeMode: 'contain'
    },
});


const mapStateToProps = state => {
    return {
    };
};

export default connect(mapStateToProps, actions)(NewsDetailsScreen);