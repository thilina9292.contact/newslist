import React, { Component } from 'react';
import { View, Dimensions,FlatList } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import colors from '../../../config/colors';
import * as actions from '../../../redux/actions';
import Loader from '../../../components/Loader';
import NewsListItem from '../../../components/NewsListItem';
import { Dropdown } from 'react-native-material-dropdown';
import { NEWS_SOURCE_DATA } from '../../../config/constants';

const entireScreenWidth = Dimensions.get('window').width;
const entireScreenHeight = Dimensions.get('window').height;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });

class CustomNewsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            newsCategoryList : [],
            isFetching: false,
            category: 'entertainment',
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (this.state.isFetching) {
            this.setState({
                isFetching : false,
            });
        }
    }

    async componentDidMount() {
        const category=this.state.category;
        this.fetchData(category);
    }

    fetchData(category) {
        this.props.getCategotryNewsData(category);
    }

    onRefresh() {
        this.setState({ isFetching: true,}, function() {
           let category = this.state.category;
           this.fetchData(category);
        });
       
    }

    renderItem(item) {
        return (
            <NewsListItem
                newsName={`${item.item.description}`}
                newsImage={''}
                defaultImage ={true}
                onPress={() => this.onNewsSelect(item)}
            />
        );
    }

    onNewsSelect(item) {
        console.log("newsItem",item);
        this.props.navigation.navigate('NewsDetails', {
            newsItem: item,
        })
    }

    onFilter(category) {
        
        this.setState({
            category: category
        }, () => {
           let category = this.state.category;
           this.fetchData(category);
        })
    }

    render() {
        let catagoryNewsLoading = this.props.catagoryNewsLoading;
        let catagoryNews = this.props.catagoryNews;
        let isFetching = this.state.isFetching;

        const data = NEWS_SOURCE_DATA;

        return (
            <View style={styles.container}>
                <View style={{flex:1}}>
                    <Dropdown
                        data={data}
                        baseColor={colors.black}
                        value={this.state.category}
                        onChangeText={(value => this.onFilter(value))}
                        dropdownPosition={0}
                        itemColor={colors.textGray}
                        textColor={colors.black}
                        fontSize={EStyleSheet.value('15rem')}
                        labelFontSize={EStyleSheet.value('15rem')}
                        inputContainerStyle={{ borderBottomColor: 'transparent', marginBottom: Platform.OS === 'ios' ? 5 : 7 }}
                        containerStyle={styles.dropdownContainer}
                        pickerStyle={{
                            width: Platform.OS === 'ios' ? EStyleSheet.value('300rem') : EStyleSheet.value('350rem'),
                            position: 'absolute',
                            left: Platform.OS === 'ios' ? 65 : 23,
                            top: Platform.OS === 'ios' ? '18.5%' : 105,
                            borderRadius: EStyleSheet.value('16rem'),
                            }}
                    />

                </View>

                <View style={{flex:9}}>
                {
                    (
                        <FlatList
                              data={catagoryNews}
                              renderItem={this.renderItem.bind(this)}
                              showsVerticalScrollIndicator={false}
                              contentContainerStyle={{
                                 justifyContent: 'center',
                                  paddingHorizontal: EStyleSheet.value('4rem'),
                                 marginVertical: EStyleSheet.value('4rem')
                              }}
                              extraData={this.state}
                              onRefresh={() => this.onRefresh()}
                              refreshing={isFetching}
                        />
                    )
                }
                </View>
                
                {catagoryNewsLoading && <Loader/>}
            </View>
        );
    }
}

const styles = EStyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
    },
    dropdownContainer: {
        flex: 1.2,
        backgroundColor: colors.white,
        justifyContent: 'center',
        height: EStyleSheet.value('32rem'),
        borderRadius: EStyleSheet.value('16rem'),
        paddingLeft: EStyleSheet.value('10rem'),
        paddingRight: EStyleSheet.value('5rem'),
        paddingBottom: '12rem',
        elevation: 0.1,
    },
});


const mapStateToProps = state => {
    return {
        catagoryNews : state.news.catagoryNews,
        catagoryNewsLoading : state.news.catagoryNewsLoading,
    };
};

export default connect(mapStateToProps, actions)(CustomNewsScreen);