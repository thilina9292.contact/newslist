import React, { Component } from 'react';
import { ImageBackground,StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import * as actions from '../redux/actions';
import bg from '../asserts/img/spalsh.png';
import { ASYNC_STORANGE_USER_LOGGED_IN } from '../config/constants';

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
           
        };
        
    }
    componentDidMount() {
        setTimeout(() => {
            this.checkUserStatus();  
        }, 1500);
    }

    async checkUserStatus() { 
        AsyncStorage.getItem('registrationStatus').then(userLoginStatus => {
            if (userLoginStatus === 'true') {
                this.props.navigation.navigate('TopNews');
            }else{
                this.props.navigation.navigate('Auth');
            }
        });  
    }
    
    render() {
        return (
            <ImageBackground
                style={styles.backgroundImage}
                source={bg}>
            </ImageBackground>
        );
    }
};

const styles = StyleSheet.create({
    backgroundImage: {
      flex: 1,
    },
  });

const mapStateToProps = state => {
    return {
    };
};
export default connect(mapStateToProps, actions)(SplashScreen);