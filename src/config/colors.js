const colors = {
    black: '#000000',
    white: '#FFFFFF',
    primaryLight: '#054470',
    primary: '#0e70b7',
    waterMelonColor: '#DE4A4A',
    yellowTab: '#FFFF04',
    yellow: '#ffda00',
    lightBackground: '#F4F4F4',
    grey: '#D8D8D8',
    textGray: '#6e6e6e',
    textNewsDetailsHeader: '#5c5c5c',
    textNewsDetails : '#969696',
    showMoreNewsDeatls: '#7f78d2',
    lightBlue : '#c1bfe0'
};
export default colors;
