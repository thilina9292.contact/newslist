import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import fontelloConfig from './selection.json';

const CustomeIcon = createIconSetFromIcoMoon(fontelloConfig);

export { CustomeIcon };
