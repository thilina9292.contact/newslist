const strings = {
    user: {
        drawerHeaderOne: 'Welcome to News',
        drawerHeaderTwo: 'by Thilina',
        logout: 'Log out',
        country : 'Sri Lanka',
        gender : 'Male',
        
    },
    toastMessage: {
        userDeatilsError: 'Enter User Credentials',
        userDeatilsEmailError: 'Enter Valid Email',
        userloginSuccess : 'Login Successful'
    },
    news :{
        newsDetailsData : 'News Details',
        newsDetailsDataTitle : 'NEWS',
        newsDetailsOpenURL : 'Click to view more'
    }
};

export default strings;

