export const ASYNC_STORANGE_USER_EMAIL = '@UserLoginMail:key';
export const ASYNC_STORANGE_USER_LOGGED_IN = '@UserLoginStatus:key';
export const REG_EMAIL_ADDRESS = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+[^<>()\.,;:\s@\"]{2,})$/i;;

export const NEWS_SOURCE_DATA = [
    {
        id: 'entertainment',
        value: 'entertainment',
    },
    {
        id: 'general',
        value: 'general',
    },
    {
        id: 'health',
        value: 'health',
    },
    {
        id: 'science',
        value: 'science',
    },
    {
        id: 'sports',
        value: 'sports',
    },
    {
        id: 'technology',
        value: 'technology',
    }
];
