/* eslint-disable no-lonely-if */
import React, { Component, PureComponent } from 'react';
import { Text, View, Dimensions, TouchableOpacity, Image,ActivityIndicator } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import FadeIn from 'react-native-fade-in-image';

import {
    Card,
} from '.';
import colors from '../config/colors';
import * as actions from '../redux/actions';
import newsDefaultImg from '../asserts/img/defult_news.png';
import {
    SkypeIndicator,
} from 'react-native-indicators';

const Placeholder = () => (
    <View style={styles.landing}>
      <SkypeIndicator color={colors.primary}/>
    </View>
);


const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });

class NewsListItem extends Component {
    constructor() {
        super();
        this.state = {
            isImageLoading: true,
        };
    }

    render() {
        const {
            newsName,
            newsImage,
            defaultImage,
            onPress
        } = this.props;

        

        return (
            <View style={{ justifyContent: 'center', flex: 1 }}>
                <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
                    <Card containerStyle={styles.mainContainer}>
                        <View style={styles.imageContainer}>
                            {
                                defaultImage ?
                                 <Image
                                        source={newsDefaultImg}
                                        resizeMode={'center'}
                                        style={[styles.image, {}]}
                                    />
                                :
                                newsImage ?
                                
                                    <FadeIn
                                        renderPlaceholderContent={ <Placeholder /> }>
                                            <Image
                                            source={{uri: newsImage}}
                                            resizeMode={'center'}
                                            style={[styles.image]}
                                            />
                                    </FadeIn>
                                    
                                    :
                                    <Image
                                        source={newsDefaultImg}
                                        resizeMode={'center'}
                                        style={[styles.image, {}]}
                                    />
                            }

                        </View>
                        <View style={styles.detailesContainer}>
                            <View style={styles.topContainer}>
                                <Text style={styles.newsName}>{newsName}</Text>
                            </View>
                            <View style={styles.middleContainer}>
                            </View>
                            <View style={styles.bottomContainer}>
                                <View style={styles.bottomRight}>
                                </View>
                            </View>
                        </View>
                    </Card>
                </TouchableOpacity>
                
            </View >
        );
    }
}

const styles = EStyleSheet.create({
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '8rem',
        marginVertical: '8rem'
    },
    imageContainer: {
        alignSelf: 'stretch',
        width: '110rem',
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    detailesContainer: {
        flex: 1,
        alignSelf: 'stretch',
        height: 'auto',
        justifyContent: 'center'
        // backgroundColor: colors.gray,
    },
    image: {
        width: '82rem',
        height: '82rem',
        alignSelf: 'center',
        resizeMode: 'contain'
    },
    topContainer: {
        height: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    middleContainer: {
        height: 'auto'
    },
    bottomContainer: {
        height: 'auto',
        flexDirection: 'row'
    },
    item: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: '8rem'
    },
    newsName: {
        fontSize: '12rem',
        color: colors.gray,
        alignSelf: 'flex-end',
        paddingTop: '10rem',
        paddingBottom: '5rem',
        fontFamily: 'HelveticaNeueMedium',
        fontWeight: '300',
    },
    bottomRight: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    landing: {
        flex: 1, alignItems: 'center', justifyContent: 'center' 
    }
});

const mapStateToProps = state => {
    return {
    };
};

export default connect(mapStateToProps, actions)(NewsListItem);
