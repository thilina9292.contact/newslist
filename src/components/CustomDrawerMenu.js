import React, {} from 'react';
import {
    Dimensions,
    SafeAreaView,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    Text
} from 'react-native';
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import * as actions from '../redux/actions';
import colors from '../config/colors';
import { Icon } from 'native-base';
import strings from '../config/strings';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../services/NavigationService';

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });


const CustomDrawerContentComponent = props => (
    <ScrollView>
        <SafeAreaView
            style={styles.container}
            forceInset={{ top: 'always', horizontal: 'never' }}>
            <TouchableOpacity style={styles.header}>
                <View style={styles.headerLeft}>
                    <Image
                        source={require('../asserts/img/user.png')}
                        style={styles.thumbnail}
                    />
                </View>
                <View style={styles.headerRight}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textName}>{strings.user.drawerHeaderOne}</Text>
                    </View>
                    <Text style={styles.textEmail}>{strings.user.drawerHeaderTwo}</Text>
                </View>

            </TouchableOpacity>
            <DrawerNavigatorItems {...props} />
            <TouchableOpacity style={styles.logoutButton} onPress={() => { NavigationService.navigate('Auth');  ; AsyncStorage.clear()}}>
                <Icon name={'face'} type={'MaterialIcons'} style={{ fontSize: EStyleSheet.value('20rem'), color: colors.white, }} />
                <Text style={styles.text}>Log out</Text>
            </TouchableOpacity>
        </SafeAreaView>
    </ScrollView>
);

const styles = EStyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: '150rem',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    thumbnail: {
        width: '71rem',
        height: '71rem',
        borderRadius: '50rem'
    },
    headerLeft: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerRight: {
        flex: 2
    },
    textName: {
        color: colors.white,
        fontFamily: 'HelveticaNeueMedium',
        fontSize: '16rem',
        fontWeight: Platform.OS === 'ios' ? '500' : '400',
    },
    textEmail: {
        opacity: 0.75,
        color: 'rgba(255, 255, 255, 0.5)',
        fontFamily: 'HelveticaNeuelight',
        fontSize: '15rem',
        fontWeight: Platform.OS === 'ios' ? '500' : '400',
    },
    text: {
        opacity: 0.5,
        color: '#ffffff',
        fontFamily: 'HelveticaNeueMedium',
        fontSize: 14.5, 
        fontWeight: 'bold',
        marginLeft: 34
    },
    logoutButton: {
        flexDirection: 'row',
        paddingHorizontal: 18, 
        marginTop: 10,
    }
});

const mapStateToProps = state => {
    return {
    };
};

export default connect(mapStateToProps, actions)(CustomDrawerContentComponent);
