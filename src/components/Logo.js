import React from 'react';
import { Image, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import logo from '../asserts/img/live.png'

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({ $rem: entireScreenWidth / 380 });

const Logo = ({ style = {} }) => {
    return (
        <Image source={logo} style={[styles.image, style]} />
    );
};

const styles = EStyleSheet.create({
    image: {
        width: '152rem',
        height: '90rem',
        resizeMode: 'contain',
    }
});

export { Logo };
