import {
    TOP_NEWS_DATA,
    TOP_NEWS_DATA_SUCCESS,
    TOP_NEWS_DATA_FAILED,
    CATEGORY_NEWS_DATA,
    CATEGORY_NEWS_DATA_SUCCESS,
    CATEGORY_NEWS_DATA_FAILED
} from '../types';

const INITIAL_STATE = {
    topNewsLoading: false,
    topNews: [],
    topNewsTotalResults : 0,
    catagoryNewsLoading: false,
    catagoryNews: [],
};


export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TOP_NEWS_DATA:
            return { ...state, topNewsLoading: true };articles
        case TOP_NEWS_DATA_SUCCESS:
            return { ...state, topNewsLoading: false,topNews: action.payload.articles,topNewsTotalResults:action.payload.totalResults};
        case TOP_NEWS_DATA_FAILED:
            return { ...state, topNewsLoading: false };
        case CATEGORY_NEWS_DATA:
            return { ...state, catagoryNewsLoading: true };
        case CATEGORY_NEWS_DATA_SUCCESS:
            return { ...state, catagoryNewsLoading: false,catagoryNews: action.payload.sources};
        case CATEGORY_NEWS_DATA_FAILED:
            return { ...state, catagoryNewsLoading: false };
        default:
            return state;
    }
};
