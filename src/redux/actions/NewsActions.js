

import {
    TOP_NEWS_DATA,
    TOP_NEWS_DATA_SUCCESS,
    TOP_NEWS_DATA_FAILED,
    CATEGORY_NEWS_DATA,
    CATEGORY_NEWS_DATA_SUCCESS,
    CATEGORY_NEWS_DATA_FAILED
} from '../types';
import APIServices from '../../services/APIServices';

export const getTopNewsData = (page) => {
    return (dispatch) => {
        dispatch({ type: TOP_NEWS_DATA });
        APIServices.getTopNews(page).then(response => {
            console.log("response",response)
            dispatch({ type: TOP_NEWS_DATA_SUCCESS, payload: response });  
        }).catch((err) => {
            dispatch({ type: TOP_NEWS_DATA_FAILED });
        });
    };
};

export const getCategotryNewsData = (catagory) => {
    return (dispatch) => {
        dispatch({ type: CATEGORY_NEWS_DATA });
        APIServices.getCatagoryNews(catagory).then(response => {
            dispatch({ type: CATEGORY_NEWS_DATA_SUCCESS, payload: response });  
        }).catch((err) => {
            dispatch({ type: CATEGORY_NEWS_DATA_FAILED });
        });
    };
};





