import request from './ApiCentral';
import {
    TOP_NEWS,
    CATEGORY_NEWS
} from '../api/API';


//news--------------------------------------------------------------------------------------
function getTopNews(page) {
    return request({
        url: TOP_NEWS+'&page='+page,
        method: 'GET',
    }, false);
}

function getCatagoryNews(catagory) {
    return request({
        url: CATEGORY_NEWS+'&category='+catagory,
        method: 'GET',
    }, false);
}


const APIServices = {
    getTopNews,
    getCatagoryNews 
};

export default APIServices;
