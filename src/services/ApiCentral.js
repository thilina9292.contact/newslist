import axios from 'axios';
import { BASE_URL} from '../api/API';


const request = async function (options, isHeader) {
    const client = axios.create({
        baseURL: BASE_URL,
        timeout: 90000,
    });

    const onSuccess = function (response) {
        console.log('Request Successful!', response);
        console.log('Request Successful time --------------------',new Date())
        return response.data;
    };

    const onError = function (error) {
        console.log('Request Failed:', error.config);
        if (error.response) {
            console.log('Status:', error.response.status);
            console.log('Data:', error.response.data);
            console.log('Headers:', error.response.headers);
        } else {
            console.log('Error Message:', error.message);
        }
        return Promise.reject(error.response || error.message);
    };

    return client(options)
        .then(onSuccess)
        .catch(onError);
};

export default request;
